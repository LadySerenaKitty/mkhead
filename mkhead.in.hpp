#ifndef kwg_hpp
#define kwg_hpp

#define MKH_NAME "@PROJECT_NAME@"
#define MKH_VERSION "@PROJECT_VERSION@"

#define MKH_BUILD "@CMAKE_BUILD_TYPE@"

#include <string>

#endif
