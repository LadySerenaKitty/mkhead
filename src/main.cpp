#include "mkhead.hpp"

#include <fstream>
#include <iostream>
#include <regex>

int main(int argc, char ** argv) {

	if (argc >= 2) {
		for (int a = 1; a < argc; a++) {
			std::regex re("[-~!@#$%^&*?+=.,;:'\\(\\)\\[\\]\\{\\}\"\\/\\\\]");
			std::string name = std::regex_replace(argv[a], re, "_");
			std::cout << argv[a] << " -> ";

			std::ifstream ifile(argv[a]);
			if (ifile.is_open()) {
				ifile.close();
				std::cout << "file already exists!";
			}
			else {
				std::ofstream ofile(argv[a]);
				if (ofile.is_open()) {
					ofile << "#ifndef " << name << "\n";
					ofile << "#define " << name << "\n";
					ofile << "\n\n";
					ofile << "#endif /* " << name << " */\n";
					ofile.flush();
					ofile.close();
					std::cout << name;
				}
				else { std::cout << "failure.  does the folder exist?"; }
			}
			std::cout << "\n";
		}
	}

	return 0;
}
