pipeline {
	agent any

	stages {
		stage("Build Config") {
			steps {
				buildName '${GIT_REVISION,length=8}-${GIT_BRANCH}'
			}
		}
		stage("Git Mining") {
			parallel {
				stage("Git Reference Build") {
					steps {
						discoverGitReferenceBuild()
					}
				}
				stage("Repository Mining") {
					steps {
						mineRepository()
					}
				}
				stage("Git DiffStat") {
					steps {
						gitDiffStat()
					}
				}
			}
		}

		stage("Project Configure") {
			parallel {
				stage("Project Configure (Debug)") {
					when {
						changeRequest target: "master"
					}
					steps {
						sh "cmake . -DCMAKE_MAKE_PROGRAM=`which make` -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
						recordIssues publishAllIssues: true, tools: [cmake()]
					}
				}
				stage("Project Configure (RelWithDebInfo)") {
					when {
						branch "master"
					}
					steps {
						sh "cmake . -DCMAKE_MAKE_PROGRAM=`which make` -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
						recordIssues publishAllIssues: true, tools: [cmake()]
					}
				}
				stage("Project Configure (Release)") {
					when {
						tag "v*.*"
					}
					steps  {
						sh "cmake . -DCMAKE_MAKE_PROGRAM=`which make` -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
						recordIssues publishAllIssues: true, tools: [cmake()]
					}
				}
			}
		}

		stage("Project Build") {
			steps {
				sh "make"
				recordIssues publishAllIssues: true, tools: [clang()]
			}
		}

		stage("Package") {
			steps {
				sh "cpack"
			}
		}

		stage("Publish Package") {
			when {
				tag 'v*.*'
			}
			steps {
				publishGiteaAssets assets: 'pkg/*.pkg', followSymlinks: false, onlyIfSuccessful: true
			}
		}

	}
	post {
		success {
			archiveArtifacts artifacts: 'pkg/*.pkg', fingerprint: true, onlyIfSuccessful: true
			archiveArtifacts artifacts: 'build/**', fingerprint: true, onlyIfSuccessful: true
		}
	}
}

