# mkhead
Simple tool to create C/C++ header files using the `ifndef`/`define` method.

The `ifndef` and `define` methods use the provided path of the file within your
project to construct the defined name, so there's no need to worry about false
equivalency in a large project.

## Building
Requirements:
* cmake

```sh
cmake .
make
doas make install
```

## Usage
```txt
mkhead [path ...]
```

Any number of file paths can be provided,
and it will make headers for all of them.

For example, `mkhead util/sodium.hpp util/chlorine.hpp` will generate two
files, with the following contents:

`util/sodium.hpp`:
```cpp
#ifndef util_sodium_hpp
#define util_sodium_hpp


#endif

```

`util/chlorine.hpp`:
```cpp
#ifndef util_chlorine_hpp
#define util_chlorine_hpp


#endif

```
